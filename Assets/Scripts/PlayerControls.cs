using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    [Header("General Setup Settings")]
    [Tooltip("How fast the ship moves up and down based upon player input")] [SerializeField] float controlSpeed;
    [Tooltip("The range in which the player can move within the X axis (horizontally)")] [SerializeField] float xRange = 10f;
    [Tooltip("The range in which the player can move within the Y axis (vertically)")] [SerializeField] float yRange = 7f;

    [Header("Laser gun array")]
    [Tooltip("Add all player lasers here")] [SerializeField] GameObject[] lasers;
    [Header("Screen position based tuning")]
    [SerializeField] float positionPitchFactor = -2f;
    [SerializeField] float controlPitchFactor = -10f;
    [Header("Player input based tuning")]
    [SerializeField] float positionYawFactor = 2f;
    [SerializeField] float controlRollFactor = -20f;
    
    
    // Declaring the variables outside the method Process Translation so that they can be accessed by other methods inside Update
    float xThrow;
    float yThrow;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ProcessTranslation();
        ProcessRotation();
        ProcessFiring();

    }

    void ProcessRotation()
    {
        // Created more variables to improve readability 
        // The float pitch has a 1st part related to the position on the screen and a 2nd part related to controls of the player
        float pitchDueToPosition = transform.localPosition.y * positionPitchFactor;
        float pitchDueToControlThrow = yThrow * controlPitchFactor;

        
        float pitch = pitchDueToPosition + pitchDueToControlThrow;
        float yaw = transform.localPosition.x * positionYawFactor;
        float roll = xThrow * controlRollFactor;
        transform.localRotation = Quaternion.Euler(pitch, yaw, roll); 
    }

    void ProcessTranslation()
    {
        // Get the movement key inputs for x and y

        xThrow = Input.GetAxis("Horizontal"); 
        yThrow = Input.GetAxis("Vertical");

        // Create some position offset to make the ship movement more realistic
        // Grab the current position, add the movement order for x or y, apply a tranformation affected by time on key press, with a field to control speed
        // Restrain the amount of movement that each key press will make so that the ship doesn't move too much offtrack, using Mathf.Clamp w/ a range field
        float xOffset = xThrow * Time.deltaTime * controlSpeed;
        float rawXPos = transform.localPosition.x + xOffset;
        float clampedXPos = Mathf.Clamp(rawXPos, -xRange, xRange);

        float yOffset = yThrow * Time.deltaTime * controlSpeed;
        float rawYPos = transform.localPosition.y + yOffset;
        float clampedYPos = Mathf.Clamp(rawYPos, -yRange, yRange);

        transform.localPosition = new Vector3(clampedXPos, clampedYPos, transform.localPosition.z);
    }

    void ProcessFiring()
    {
        // if pushing the fire button start shooting
        // else stop shooting
        if (Input.GetButton("Fire1"))
        {
            SetLasersActive(true);
        }
        else
        {
            SetLasersActive(false);
        }
    }

    void SetLasersActive(bool isActive)
    {
        // for each laser, turn them on
        // Instead of 2 processes for activating and deactivating, have a single boolean that defines active or inactive based on Fire1 input
        foreach (GameObject laser in lasers)
        {
            var emissionModule = laser.GetComponent<ParticleSystem>().emission;
            emissionModule.enabled = isActive;
        }
    }
}
